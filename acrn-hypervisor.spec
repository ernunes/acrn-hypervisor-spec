
%global ACRN_BOARD         my_board
%global ACRN_SCENARIO      scenario
%global ACRN_CONFIGURATION MyConfiguration
%global ACRN_LAUNCH_SCRIPT launch_user_vm_id1

Name:           acrn-hypervisor
Version:        3.2
Release:        1%{?dist}
Summary:        A flexible, light-weight, open source reference hypervisor for IoT devices

License:        BSD-3-Clause
URL:            https://projectacrn.org/
Source0:        https://codeload.github.com/projectacrn/acrn-hypervisor/tar.gz/refs/tags/v3.2
Source1:        %{ACRN_BOARD}.board.xml
Source2:        %{ACRN_SCENARIO}.xml
Source3:        %{ACRN_LAUNCH_SCRIPT}.sh

Patch0:         remove-udmabuf-list_limit.patch

# This patch is optional and is meant to work around a firmware issue in
# some raptorlake NUCs.
# It is not harmful to functionality in other platforms, but it can be
# optionally disabled on platforms other than the NUC if preferred.
# To disable it, just remove or comment the line below.
Patch1:         0001-ACRN-skip-VF-vendor-ID-check-on-raptorlake-NUC.patch

%global debug_package %{nil}

BuildRequires:  acpica-tools
BuildRequires:  cjson-devel
BuildRequires:  e2fsprogs-devel
BuildRequires:  gcc
BuildRequires:  git
BuildRequires:  libblkid-devel
BuildRequires:  libdrm-devel
BuildRequires:  libevent-devel
BuildRequires:  libpciaccess-devel
BuildRequires:  libusb1-devel
BuildRequires:  libxml2-devel
BuildRequires:  make
BuildRequires:  numactl-devel
BuildRequires:  openssl-devel
BuildRequires:  pixman-devel
BuildRequires:  python3-defusedxml
BuildRequires:  python3-devel
BuildRequires:  python3-elementpath
BuildRequires:  python3-lxml
BuildRequires:  SDL2-devel
BuildRequires:  systemd-devel

%description
The open source project ACRN defines a device hypervisor reference stack and an
architecture for running multiple software subsystems, managed securely, on a
consolidated system by means of a virtual machine manager. It also defines a
reference framework implementation for virtual device emulation, called the
"ACRN Device Model".

%package -n acrnd
Summary: ACRN Hypervisor control daemon
%description -n acrnd
ACRN is an open source reference hypervisor, built to meet the unique needs of
embedded IoT development. This is the ACRN daemon for hypervisor control.

%package -n acrn-devicemodel
Summary: Devicemodel for ACRN Hypervisor
%description -n acrn-devicemodel
ACRN is an open source reference hypervisor, built to meet the unique needs of
embedded IoT development. This is the ACRN specific devicemodel for ACRN only.

%package -n acrn-devel
Summary: Public headers and libs for ACRN
%description -n acrn-devel
ACRN is an open source reference hypervisor, built to meet the unique needs of
embedded IoT development. This package contains the public headers and static
libraries for ACRN.

%package -n acrn-lifemngr
Summary: ACRN life manager
%description -n acrn-lifemngr
ACRN is an open source reference hypervisor, built to meet the unique needs of
embedded IoT development. This package contains the helper service for Linux
VMs to provide proper shutdown/suspend

%package -n acrn-tools
Summary: Supplementary tools for ACRN Hypervisor on AMD64
%description -n acrn-tools
ACRN is an open source reference hypervisor, built to meet the unique needs of
embedded IoT development. These are ACRN specific additional tools meant for
debug use only.

%package -n grub-acrn
Summary: Grub setup scripts for ACRN Hypervisor
Requires: grub2-efi-x64-modules
%description -n grub-acrn
This package provides additional scripts to fit into the Grub bootloader config
creation. It supports generating the respective Grub boot entries for an ACRN
system.

%prep
%autosetup -p1

%build
# we couldn't pass a source in a subdirectory but the build system uses the
# directory path as an input, so re-create the expected directory path.
mkdir -p %{ACRN_CONFIGURATION}/
cp %{SOURCE1} %{SOURCE2} %{ACRN_CONFIGURATION}/
%make_build BOARD=%{ACRN_CONFIGURATION}/%{ACRN_BOARD}.board.xml SCENARIO=%{ACRN_CONFIGURATION}/%{ACRN_SCENARIO}.xml all hypervisor devicemodel tools life_mngr

%install
%make_install BOARD=%{ACRN_CONFIGURATION}/%{ACRN_BOARD}.board.xml SCENARIO=%{ACRN_CONFIGURATION}/%{ACRN_SCENARIO}.xml install life_mngr-install hypervisor-install devicemodel-install tools-install

mkdir -p %{buildroot}%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}
install -m 0644 build/hypervisor/acrn.map          %{buildroot}%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/acrn.%{ACRN_BOARD}.%{ACRN_CONFIGURATION}.map
install -m 0644 build/hypervisor/acrn.bin          %{buildroot}%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/acrn.%{ACRN_BOARD}.%{ACRN_CONFIGURATION}.bin
install -m 0644 build/hypervisor/configs/config.mk %{buildroot}%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/acrn.%{ACRN_BOARD}.%{ACRN_CONFIGURATION}.config

# TODO: Should this be a post install script?
mkdir -p %{buildroot}/boot
install -m 0644 build/hypervisor/acrn.map          %{buildroot}/boot/acrn-%{version}.map
install -m 0644 build/hypervisor/acrn.bin          %{buildroot}/boot/acrn-%{version}.bin
install -m 0644 build/hypervisor/configs/config.mk %{buildroot}/boot/acrn-%{version}.config

mkdir -p %{buildroot}%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}%{_sysconfdir}
#install -m 0644 build/hypervisor/serial.conf       %{buildroot}%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}%{_sysconfdir}/serial.conf
mv %{buildroot}%{_sysconfdir}/serial.conf %{buildroot}%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}%{_sysconfdir}/serial.conf

install -m 0644 %{SOURCE1} %{buildroot}%{_libdir}/acrn/%{ACRN_BOARD}/board.xml
install -m 0644 %{SOURCE2} %{buildroot}%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/%{ACRN_SCENARIO}.xml

mkdir -p %{buildroot}%{_datadir}/acrn/launch-scripts/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/
cp %{SOURCE3} %{buildroot}%{_datadir}/acrn/launch-scripts/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/

mv %{buildroot}%{_bindir}/{life_mngr,acrn-lifemngr}
mv %{buildroot}%{_unitdir}/{life_mngr.service,acrn-lifemngr.service}

# install life_mngr
mkdir -p %{buildroot}%{_datadir}/acrn-lifemngr/
mv %{buildroot}%{_sysconfdir}/life_mngr/life_mngr.conf %{buildroot}%{_datadir}/acrn-lifemngr/life_mngr.conf.service_vm
install -m 0644 debian/lifemngr/life_mngr.conf.user_vm %{buildroot}%{_datadir}/acrn-lifemngr/life_mngr.conf.user_vm
install -m 0755 debian/lifemngr/start-acrn-lifemngr.sh %{buildroot}%{_datadir}/acrn-lifemngr/start-acrn-lifemngr.sh
sed -i -e 's#^ExecStart=.*#ExecStart=$(datadir)/acrn-lifemngr/start-acrn-lifemngr.sh#' %{buildroot}%{_unitdir}/acrn-lifemngr.service

mkdir -p %{buildroot}%{_sysconfdir}/default/
install -m 0644 debian/acrnlog/acrnlog.default %{buildroot}%{_sysconfdir}/default/acrnlog

mkdir -p %{buildroot}%{_datadir}/acrn/scripts
cp -a misc/debug_tools/acrn_trace/scripts/* %{buildroot}%{_datadir}/acrn/scripts/

mkdir -p %{buildroot}%{_datadir}/defaults/telemetrics
install -m 0644 misc/debug_tools/acrn_crashlog/data/acrnprobe.xml %{buildroot}%{_datadir}/defaults/telemetrics/acrnprobe.xml

mkdir -p %{buildroot}%{_sysconfdir}/grub.d/
install -m 0755 debian/grub/25_linux_acrn %{buildroot}%{_sysconfdir}/grub.d/25_linux_acrn
mkdir -p %{buildroot}%{_sysconfdir}/default/grub.d/
install -m 0644 debian/grub/acrn.cfg %{buildroot}%{_sysconfdir}/default/grub.d/acrn.cfg

# patch out debian-specific code
sed -i "s#^ACRN_BOARD=.*#ACRN_BOARD=%{ACRN_BOARD}#g"               %{buildroot}%{_sysconfdir}/grub.d/25_linux_acrn
sed -i "s#^ACRN_SCENARIO=.*#ACRN_SCENARIO=%{ACRN_CONFIGURATION}#g" %{buildroot}%{_sysconfdir}/grub.d/25_linux_acrn
sed -i "s#/usr/lib/x86_64-linux-gnu#/usr/lib64#g"                  %{buildroot}%{_sysconfdir}/grub.d/25_linux_acrn
sed -i "s#grub-file#grub2-file#g"                                  %{buildroot}%{_sysconfdir}/grub.d/25_linux_acrn
sed -i '4 iecho insmod multiboot2'                                 %{buildroot}%{_sysconfdir}/grub.d/25_linux_acrn
# workaround for the debconf check line
sed -i -e '/.*awk.*/,+5d'                                          %{buildroot}%{_sysconfdir}/grub.d/25_linux_acrn

%files -n acrnd
%{_bindir}/acrnctl
%{_bindir}/acrnd
%{_datadir}/acrn/
%{_unitdir}/../network/50-acrn.netdev
%{_unitdir}/../network/50-acrn.network
%{_unitdir}/../network/50-eth.network
%{_unitdir}/../network/50-tap0.netdev
%{_unitdir}/acrnd.service

%files -n acrn-devel
%{_includedir}/acrn/acrn_common.h
%{_includedir}/acrn/acrn_mngr.h
%{_includedir}/acrn/dm.h
%{_includedir}/acrn/dm_string.h
%{_includedir}/acrn/hsm_ioctl_defs.h
%{_includedir}/acrn/macros.h
%{_includedir}/acrn/pm.h
%{_includedir}/acrn/types.h
%{_libdir}/libacrn-mngr.a

%files -n acrn-devicemodel
%{_bindir}/acrn-dm
%{_datadir}/acrn/bios/MD5SUM_ovmf
%{_datadir}/acrn/bios/MD5SUM_vsbl
%{_datadir}/acrn/bios/OVMF.fd
%{_datadir}/acrn/bios/OVMF_CODE.fd
%{_datadir}/acrn/bios/OVMF_CODE_debug.fd
%{_datadir}/acrn/bios/OVMF_VARS.fd
%{_datadir}/acrn/bios/OVMF_debug.fd
%{_datadir}/acrn/bios/SHA512SUM_ovmf
%{_datadir}/acrn/bios/SHA512SUM_vsbl
%{_datadir}/acrn/bios/VSBL.bin
%{_datadir}/acrn/bios/VSBL_debug.bin
%{_datadir}/acrn/bios/changelog_ovmf.txt
%{_datadir}/acrn/bios/changelog_vsbl.txt

%files -n acrn-hypervisor
%{_libdir}/acrn/acrn.%{ACRN_BOARD}.%{ACRN_CONFIGURATION}.32.out
%{_libdir}/acrn/acrn.%{ACRN_BOARD}.%{ACRN_CONFIGURATION}.bin
%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/acrn.%{ACRN_BOARD}.%{ACRN_CONFIGURATION}.bin
%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/acrn.%{ACRN_BOARD}.%{ACRN_CONFIGURATION}.config
%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/acrn.%{ACRN_BOARD}.%{ACRN_CONFIGURATION}.map
%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}%{_sysconfdir}/serial.conf
%{_libdir}/acrn/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/%{ACRN_SCENARIO}.xml
%{_libdir}/acrn/%{ACRN_BOARD}/board.xml
%{_datadir}/acrn/launch-scripts/%{ACRN_BOARD}/%{ACRN_CONFIGURATION}/%{ACRN_LAUNCH_SCRIPT}.sh
/boot/acrn-%{version}.map
/boot/acrn-%{version}.bin
/boot/acrn-%{version}.config

%files -n acrn-lifemngr
%{_unitdir}/acrn-lifemngr.service
%{_bindir}/acrn-lifemngr
%{_datadir}/acrn-lifemngr/life_mngr.conf.service_vm
%{_datadir}/acrn-lifemngr/life_mngr.conf.user_vm
%{_datadir}/acrn-lifemngr/start-acrn-lifemngr.sh

%files -n acrn-tools
%{_sysconfdir}/default/acrnlog
%{_unitdir}/acrnlog.service
%{_unitdir}/acrnprobe.service
%{_unitdir}/usercrash.service
%{_bindir}/acrnlog
%{_bindir}/acrnprobe
%{_bindir}/acrntrace
%{_bindir}/crashlogctl
%{_bindir}/debugger
%{_bindir}/usercrash-wrapper
%{_bindir}/usercrash_c
%{_bindir}/usercrash_s
%{_datadir}/acrn/crashlog/40-watchdog.conf
%{_datadir}/acrn/crashlog/80-coredump.conf
%{_datadir}/acrn/scripts/acrnalyze.py
%{_datadir}/acrn/scripts/acrntrace_format.py
%{_datadir}/acrn/scripts/formats
%{_datadir}/acrn/scripts/irq_analyze.py
%{_datadir}/acrn/scripts/vmexit_analyze.py
%{_datadir}/defaults/telemetrics/acrnprobe.xml

%files -n grub-acrn
%{_sysconfdir}/default/grub.d/acrn.cfg
%{_sysconfdir}/grub.d/25_linux_acrn

%changelog
* Thu May 02 2024 Erico Nunes <ernunes@redhat.com>
- Initial build
